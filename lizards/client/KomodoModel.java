/*
 * KomodoModel.java
 *
 *  Copyright (c) 2017 Michael Sheppard
 *
 * =====GPLv3===========================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.lizards.client;

import com.google.common.collect.ImmutableList;
import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

import javax.annotation.Nonnull;

public class KomodoModel<T extends Entity> extends SegmentedModel<T> {

    private ModelRenderer komodoBody;
    private ModelRenderer komodoHead;
    private ModelRenderer komodoLeg1;
    private ModelRenderer komodoLeg2;
    private ModelRenderer komodoLeg3;
    private ModelRenderer komodoLeg4;
    private ModelRenderer komodoTail;

    public KomodoModel() {
        float yPos = 16F;

        komodoBody = new ModelRenderer(this, 12, 8);
        komodoBody.addBox(-3F, -3F, -8F, 6, 8, 16);
        komodoBody.setRotationPoint(0.0F, yPos, 0.0F);

        komodoHead = new ModelRenderer(this, 0, 0);
        komodoHead.addBox(-3F, -3F, -8F, 6, 6, 8);
        komodoHead.setRotationPoint(0.0F, yPos, -8F);

        komodoLeg1 = new ModelRenderer(this, 48, 0);
        komodoLeg1.addBox(-2F, 0.0F, -2F, 4, 8, 4);
        komodoLeg1.setRotationPoint(5F, yPos, -5F);

        komodoLeg2 = new ModelRenderer(this, 48, 0);
        komodoLeg2.addBox(-2F, 0.0F, -2F, 4, 8, 4);
        komodoLeg2.setRotationPoint(5F, yPos, 5F);

        komodoLeg3 = new ModelRenderer(this, 48, 0);
        komodoLeg3.mirror = true;
        komodoLeg3.addBox(-2F, 0.0F, -2F, 4, 8, 4);
        komodoLeg3.setRotationPoint(-5F, yPos, -5F);

        komodoLeg4 = new ModelRenderer(this, 48, 0);
        komodoLeg4.mirror = true;
        komodoLeg4.addBox(-2F, 0.0F, -2F, 4, 8, 4);
        komodoLeg4.setRotationPoint(-5F, yPos, 5F);

        komodoTail = new ModelRenderer(this, 16, 8);
        komodoTail.addBox(-2F, -2F, 0.0F, 4, 4, 20);
        komodoTail.setRotationPoint(0.0F, yPos, 6F);
        komodoTail.rotateAngleX = 5.934119F;
    }

    @Override
    public void setRotationAngles(@Nonnull T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
        komodoHead.rotateAngleX = headPitch / 57.29578F;
        komodoHead.rotateAngleY = headPitch / 57.29578F;

        komodoLeg1.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
        komodoLeg2.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
        komodoLeg3.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
        komodoLeg4.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;

        komodoTail.rotateAngleY = MathHelper.cos(limbSwing * 0.6662F) * 0.4F * limbSwingAmount;
    }

    @Override
    @Nonnull
    public Iterable<ModelRenderer> getParts() {
        return ImmutableList.of(komodoHead, komodoBody, komodoLeg1, komodoLeg2, komodoLeg3, komodoLeg4, komodoTail);
    }

}
