/*
 * IguanaModel.java
 *
 *  Copyright (c) 2017 Michael Sheppard
 *
 * =====GPLv3===========================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.lizards.client;

import com.google.common.collect.ImmutableList;
import com.lizards.common.IguanaEntity;
import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;

import javax.annotation.Nonnull;

public class IguanaModel <T extends IguanaEntity> extends SegmentedModel<T> {

	public ModelRenderer iguanaBody;
	public ModelRenderer iguanaHead;
	public ModelRenderer iguanaLeg1;
	public ModelRenderer iguanaLeg2;
	public ModelRenderer iguanaLeg3;
	public ModelRenderer iguanaLeg4;
	ModelRenderer iguanaTail;

	public IguanaModel()
	{
		float yPos = 19F;

		iguanaBody = new ModelRenderer(this, 21, 16);
		iguanaBody.addBox(-3F, -2F, -5F, 6, 4, 10);
		iguanaBody.setRotationPoint(0.0F, yPos, 0.0F);

		iguanaHead = new ModelRenderer(this, 0, 0);
		iguanaHead.addBox(-2F, -2F, -6F, 4, 4, 6);
		iguanaHead.setRotationPoint(0F, yPos, -5F);

		iguanaLeg1 = new ModelRenderer(this, 56, 1);
		iguanaLeg1.addBox(-1F, 0F, -1F, 2, 5, 2);
		iguanaLeg1.setRotationPoint(4F, yPos, -4F);

		iguanaLeg2 = new ModelRenderer(this, 56, 1);
		iguanaLeg2.addBox(-1F, 0F, -1F, 2, 5, 2);
		iguanaLeg2.setRotationPoint(4F, yPos, 4F);

		iguanaLeg3 = new ModelRenderer(this, 56, 1);
		iguanaLeg3.mirror = true;
		iguanaLeg3.addBox(-1F, 0F, -1F, 2, 5, 2);
		iguanaLeg3.setRotationPoint(-4F, yPos, -4F);

		iguanaLeg4 = new ModelRenderer(this, 56, 1);
		iguanaLeg4.mirror = true;
		iguanaLeg4.addBox(-1F, 0F, -1F, 2, 5, 2);
		iguanaLeg4.setRotationPoint(-4F, yPos, 4F);

		iguanaTail = new ModelRenderer(this, 17, 12);
		iguanaTail.addBox(-1F, -1F, 0F, 2, 2, 18);
		iguanaTail.setRotationPoint(0F, yPos, 4F);
		iguanaTail.rotateAngleX = 6.021385919380437F;
	}

	@Override
	public void setRotationAngles(@Nonnull T p_225597_1_, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
		iguanaHead.rotateAngleX = headPitch / 57.29578F;
		iguanaHead.rotateAngleY = headPitch / 57.29578F;

		iguanaLeg1.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
		iguanaLeg2.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
		iguanaLeg3.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
		iguanaLeg4.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;

		iguanaTail.rotateAngleY = MathHelper.cos(limbSwing * 0.6662F) * 0.4F * limbSwingAmount;
	}

	@Override
	@Nonnull
	public Iterable<ModelRenderer> getParts() {
		return ImmutableList.of(iguanaHead, iguanaBody, iguanaLeg1, iguanaLeg2, iguanaLeg3, iguanaLeg4, iguanaTail);
	}

}
