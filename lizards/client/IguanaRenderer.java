/*
 * IguanaRenderer.java
 *
 *  Copyright (c) 2017 Michael Sheppard
 *
 * =====GPLv3===========================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.lizards.client;

import com.lizards.common.IguanaEntity;
import com.lizards.common.LizardMod;
import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;

import javax.annotation.Nonnull;

public class IguanaRenderer<T extends IguanaEntity> extends MobRenderer<T, IguanaModel<T>> {

    private static final ResourceLocation[] SKIN = new ResourceLocation[]{
            new ResourceLocation(LizardMod.MODID, "textures/entity/lizards/iguana0.png"),
            new ResourceLocation(LizardMod.MODID, "textures/entity/lizards/iguana1.png"),
            new ResourceLocation(LizardMod.MODID, "textures/entity/lizards/iguana2.png"),
            new ResourceLocation(LizardMod.MODID, "textures/entity/lizards/iguana3.png")
    };
    float scaleFactor = 0.5F;

    public IguanaRenderer(EntityRendererManager rm) {
        super(rm, new IguanaModel<>(), 0.0f);
    }

    @Override
    protected void preRenderCallback(T entity, MatrixStack matrixStack, float unknown) {
        matrixStack.scale(scaleFactor, scaleFactor, scaleFactor);
    }

    @Override
    @Nonnull
    public ResourceLocation getEntityTexture(@Nonnull T entity) {
        return SKIN[entity.getVariant()];
    }

}
