/*
 * ChameleonModel.java
 *
 *  Copyright (c) 2017 Michael Sheppard
 *
 * =====GPLv3===========================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.lizards.client;

import com.google.common.collect.ImmutableList;
import com.lizards.common.ChameleonEntity;
import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.util.math.MathHelper;

import javax.annotation.Nonnull;

public class ChameleonModel<T extends ChameleonEntity> extends SegmentedModel<T> {

    public ModelRenderer chameleonBody;
    public ModelRenderer chameleonHead;
    public ModelRenderer chameleonLeg1;
    public ModelRenderer chameleonLeg2;
    public ModelRenderer chameleonLeg3;
    public ModelRenderer chameleonLeg4;
    ModelRenderer chameleonTail;

    public ChameleonModel() {
        float yPos = 19F;

        chameleonBody = new ModelRenderer(this, 21, 16);
        chameleonBody.addBox(-3F, -2F, -5F, 6, 4, 10);
        chameleonBody.setRotationPoint(0.0F, yPos, 0.0F);

        chameleonHead = new ModelRenderer(this, 0, 0);
        chameleonHead.addBox(-2F, -2F, -6F, 4, 4, 6);
        chameleonHead.setRotationPoint(0F, yPos, -5F);

        chameleonLeg1 = new ModelRenderer(this, 56, 1);
        chameleonLeg1.addBox(-1F, 0F, -1F, 2, 5, 2);
        chameleonLeg1.setRotationPoint(4F, yPos, -4F);

        chameleonLeg2 = new ModelRenderer(this, 56, 1);
        chameleonLeg2.addBox(-1F, 0F, -1F, 2, 5, 2);
        chameleonLeg2.setRotationPoint(4F, yPos, 4F);

        chameleonLeg3 = new ModelRenderer(this, 56, 1);
        chameleonLeg3.mirror = true;
        chameleonLeg3.addBox(-1F, 0F, -1F, 2, 5, 2);
        chameleonLeg3.setRotationPoint(-4F, yPos, -4F);

        chameleonLeg4 = new ModelRenderer(this, 56, 1);
        chameleonLeg4.mirror = true;
        chameleonLeg4.addBox(-1F, 0F, -1F, 2, 5, 2);
        chameleonLeg4.setRotationPoint(-4F, yPos, 4F);

        chameleonTail = new ModelRenderer(this, 17, 12);
        chameleonTail.addBox(-1F, -1F, 0F, 2, 2, 18);
        chameleonTail.setRotationPoint(0F, yPos, 4F);
        chameleonTail.rotateAngleX = 6.021385919380437F;
    }

    @Override
    public void setRotationAngles(@Nonnull T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
        chameleonHead.rotateAngleX = headPitch / 57.29578F;
        chameleonHead.rotateAngleY = headPitch / 57.29578F;

        chameleonLeg1.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
        chameleonLeg2.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
        chameleonLeg3.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
        chameleonLeg4.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;

        chameleonTail.rotateAngleY = MathHelper.cos(limbSwing * 0.6662F) * 0.4F * limbSwingAmount;
    }

    @Override
    @Nonnull
    public Iterable<ModelRenderer> getParts() {
        return ImmutableList.of(chameleonHead, chameleonBody, chameleonLeg1, chameleonLeg2, chameleonLeg3, chameleonLeg4, chameleonTail);
    }

}
