/*
 * ChameleonRenderer.java
 *
 *  Copyright (c) 2017 Michael Sheppard
 *
 * =====GPLv3===========================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.lizards.client;

import com.lizards.common.LizardMod;
import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.util.ResourceLocation;
import com.lizards.common.ChameleonEntity;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import javax.annotation.Nonnull;

@OnlyIn(Dist.CLIENT)
public class ChameleonRenderer extends MobRenderer<ChameleonEntity, ChameleonModel<ChameleonEntity>> {
    private static final ResourceLocation SKIN = new ResourceLocation(LizardMod.MODID, "textures/entity/lizards/greyscale_chameleon_p.png");
    float scaleFactor = 0.35f;

    public ChameleonRenderer(EntityRendererManager rm) {
        super(rm, new ChameleonModel<>(), 0.0f);
        addLayer(new ChameleonSkinLayer(this));
    }


    @Override
    @Nonnull
    public ResourceLocation getEntityTexture(@Nonnull ChameleonEntity entity) {
        return SKIN;
    }

    @Override
    protected void preRenderCallback(ChameleonEntity entity, MatrixStack matrixStack, float unknown) {
        matrixStack.scale(scaleFactor, scaleFactor, scaleFactor);
    }

}
