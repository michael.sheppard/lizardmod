/*
 * LaceModel.java
 *
 *  Copyright (c) 2017 Michael Sheppard
 *
 * =====GPLv3===========================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.lizards.client;

import com.google.common.collect.ImmutableList;
import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

import javax.annotation.Nonnull;


public class LaceModel<T extends Entity> extends SegmentedModel<T> {

    private ModelRenderer laceBody;
    private ModelRenderer laceHead;
    private ModelRenderer laceLeg1;
    private ModelRenderer laceLeg2;
    private ModelRenderer laceLeg3;
    private ModelRenderer laceLeg4;
    private ModelRenderer laceTail;

    public LaceModel() {
        float yPos = 19F;

        laceBody = new ModelRenderer(this, 21, 16);
        laceBody.addBox(-3F, -2F, -5F, 6, 4, 10);
        laceBody.setRotationPoint(0.0F, yPos, 0.0F);

        laceHead = new ModelRenderer(this, 0, 0);
        laceHead.addBox(-2F, -2F, -6F, 4, 4, 6);
        laceHead.setRotationPoint(0F, yPos, -5F);

        laceLeg1 = new ModelRenderer(this, 56, 1);
        laceLeg1.addBox(-1F, 0F, -1F, 2, 5, 2);
        laceLeg1.setRotationPoint(4F, yPos, -4F);

        laceLeg2 = new ModelRenderer(this, 56, 1);
        laceLeg2.addBox(-1F, 0F, -1F, 2, 5, 2);
        laceLeg2.setRotationPoint(4F, yPos, 4F);

        laceLeg3 = new ModelRenderer(this, 56, 1);
        laceLeg3.mirror = true;
        laceLeg3.addBox(-1F, 0F, -1F, 2, 5, 2);
        laceLeg3.setRotationPoint(-4F, yPos, -4F);

        laceLeg4 = new ModelRenderer(this, 56, 1);
        laceLeg4.mirror = true;
        laceLeg4.addBox(-1F, 0F, -1F, 2, 5, 2);
        laceLeg4.setRotationPoint(-4F, yPos, 4F);

        laceTail = new ModelRenderer(this, 17, 12);
        laceTail.addBox(-1F, -1F, 0F, 2, 2, 18);
        laceTail.setRotationPoint(0F, yPos, 4F);
        laceTail.rotateAngleX = 6.021385919380437F;
    }

    @Override
    public void setRotationAngles(@Nonnull T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
        laceHead.rotateAngleX = headPitch / 57.29578F;
        laceHead.rotateAngleY = headPitch / 57.29578F;

        laceLeg1.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
        laceLeg2.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
        laceLeg3.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
        laceLeg4.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;

        laceTail.rotateAngleY = MathHelper.cos(limbSwing * 0.6662F) * 0.4F * limbSwingAmount;
    }

    @Override
    @Nonnull
    public Iterable<ModelRenderer> getParts() {
        return ImmutableList.of(laceHead, laceBody, laceLeg1, laceLeg2, laceLeg3, laceLeg4, laceTail);
    }

}
