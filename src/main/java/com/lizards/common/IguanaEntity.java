/*
 * IguanaEntity.java
 *
 *  Copyright (c) 2017 Michael Sheppard
 *
 * =====GPLv3===========================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.lizards.common;

import net.minecraft.entity.*;
import net.minecraft.entity.ai.goal.*;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public final class IguanaEntity extends CreatureEntity {
    private static final DataParameter<Integer> VARIANT = EntityDataManager.createKey(IguanaEntity.class, DataSerializers.VARINT);
    private final float WIDTH = 0.30f;
    private final float HEIGHT = 0.3f;
    public EntitySize lizardSize = new EntitySize(WIDTH, HEIGHT, false);

    private static final DataParameter<Float> health = EntityDataManager.createKey(IguanaEntity.class, DataSerializers.FLOAT);

    public IguanaEntity(EntityType<? extends IguanaEntity> entity, World world) {
        super(entity, world);
        setPathPriority(PathNodeType.WATER, 0.0f);
        lizardSize.scale(WIDTH, HEIGHT);
    }

    @SuppressWarnings("unused")
    public IguanaEntity(World world) {
        this(LizardMod.RegistryEvents.IGUANA, world);
    }

    @Override
    protected void registerGoals() {
        double moveSpeed = 1.0;

        goalSelector.addGoal(1, new SwimGoal(this));
        goalSelector.addGoal(2, new PanicGoal(this, 0.38F));
        goalSelector.addGoal(6, new RandomWalkingGoal(this, moveSpeed));
        goalSelector.addGoal(7, new LookAtGoal(this, PlayerEntity.class, 6.0F));
        goalSelector.addGoal(7, new LookRandomlyGoal(this));
    }

    @Nullable
    public ILivingEntityData onInitialSpawn(IWorld worldIn, DifficultyInstance difficultyIn, SpawnReason reason, @Nullable ILivingEntityData spawnDataIn, @Nullable CompoundNBT dataTag) {
        setVariant(rand.nextInt(4));
        return super.onInitialSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
    }

    @Override
    public void tick() {
        // kill in cold biomes
        Vec3d v = getPositionVec();
        BlockPos bp = new BlockPos(v.x, v.y, v.z);
        Biome biome = world.getBiome(bp);
        if (biome.getTemperature(bp) <= 0.25) {
            attackEntityFrom(DamageSource.STARVE, 4.0f);
        }
        super.tick();
    }

    @Override
    public boolean canDespawn(double distanceToPlayer) {
        return false;
    }

    @Override
    protected void registerAttributes() {
        super.registerAttributes();
        getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(10.0); // health
        getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.2); // move speed
    }

    @Override
    protected void registerData() {
        super.registerData();
        dataManager.register(health, getHealth());
        dataManager.register(VARIANT, 0);
    }

    public int getVariant() {
        return MathHelper.clamp(dataManager.get(VARIANT), 0, 4);
    }

    public void setVariant(int variantIn) {
        dataManager.set(VARIANT, variantIn);
    }

    public void writeAdditional(CompoundNBT compound) {
        super.writeAdditional(compound);
        compound.putInt("Variant", getVariant());
    }

    public void readAdditional(CompoundNBT compound) {
        super.readAdditional(compound);
        setVariant(compound.getInt("Variant"));
    }

    @Override
    protected float getStandingEyeHeight(Pose pose, EntitySize size) {
        return HEIGHT * 0.9f;
    }

    @Nonnull
    @Override
    public EntityType<?> getType() {
        return LizardMod.RegistryEvents.IGUANA;
    }

    @Nonnull
    @Override
    public EntitySize getSize(Pose p) {
        return new EntitySize(WIDTH, HEIGHT, false);
    }

    @Nonnull
    @Override
    public ResourceLocation getLootTable() {
        return new ResourceLocation(LizardMod.MODID, LizardMod.IGUANA_NAME);
    }

    @Override
    protected float getSoundVolume() {
        return 0.4F;
    }

    @Override
    protected SoundEvent getDeathSound() {
        return LizardMod.RegistryEvents.VARANUS_HURT;
    }

    @Override
    protected void updateAITasks() {
        dataManager.set(health, getHealth());
    }

    @Override
    public boolean attackEntityAsMob(@Nonnull Entity entity) {
        return entity.attackEntityFrom(DamageSource.causeMobDamage(this), 2);
    }

}
