/*
 * ChameleonEntity.java
 *
 *  Copyright (c) 2017 Michael Sheppard
 *
 * =====GPLv3===========================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.lizards.common;

import net.minecraft.entity.*;
import net.minecraft.entity.ai.goal.*;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;

import javax.annotation.Nonnull;

public final class ChameleonEntity extends CreatureEntity {

    private final float WIDTH = 0.30f;
    private final float HEIGHT = 0.5f;
    public EntitySize lizardSize = new EntitySize(WIDTH, HEIGHT, false);
    private static final DataParameter<Float> health = EntityDataManager.createKey(ChameleonEntity.class, DataSerializers.FLOAT);

    public ChameleonEntity(EntityType<? extends ChameleonEntity> entity, World world) {
        super(entity, world);
        setHealth(10.0f);
        setPathPriority(PathNodeType.WATER, 0.0f);

        lizardSize.scale(WIDTH, HEIGHT);
    }

    @SuppressWarnings("unused")
    public ChameleonEntity(World world) {
        this(LizardMod.RegistryEvents.CHAMELEON, world);
    }

    @Override
    protected void registerGoals() {
        double moveSpeed = 1.0;

        goalSelector.addGoal(1, new SwimGoal(this));
        goalSelector.addGoal(2, new PanicGoal(this, 0.38F));
        goalSelector.addGoal(6, new RandomWalkingGoal(this, moveSpeed));
        goalSelector.addGoal(7, new LookAtGoal(this, PlayerEntity.class, 6.0F));
        goalSelector.addGoal(7, new LookRandomlyGoal(this));
    }

    @Override
    public boolean canDespawn(double distanceToPlayer) {
        return false;
    }

    @Override
    public void tick() {
        // kill in cold biomes
        Vec3d v = getPositionVec();
        BlockPos bp = new BlockPos(v.x, v.y, v.z);
        Biome biome = world.getBiome(bp);
        if (biome.getTemperature(bp) <= 0.25) {
            attackEntityFrom(DamageSource.STARVE, 4.0f);
        }
        super.tick();
    }

    @Override
    protected void registerAttributes() {
        super.registerAttributes();
        getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(10.0); // health
        getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.2); // move speed
    }

    @Override
    protected void registerData() {
        super.registerData();
        dataManager.register(health, getHealth());
    }

    @Override
    protected float getStandingEyeHeight(Pose pose, EntitySize size) {
        return HEIGHT * 0.9f;
    }

    @Nonnull
    @Override
    public EntityType<?> getType() {
        return LizardMod.RegistryEvents.CHAMELEON;
    }

    @Nonnull
    @Override
    public EntitySize getSize(Pose p) {
        return new EntitySize(WIDTH, HEIGHT, false);
    }

    @Nonnull
    @Override
    public ResourceLocation getLootTable() {
        return new ResourceLocation(LizardMod.MODID, LizardMod.CHAMELEON_NAME);
    }

    @Override
    protected float getSoundVolume() {
        return 0.4F;
    }

    @Override
    protected SoundEvent getDeathSound() {
        return LizardMod.RegistryEvents.VARANUS_HURT;
    }

    @Override
    protected void updateAITasks() {
        dataManager.set(health, getHealth());
    }

    @Override
    public boolean attackEntityAsMob(@Nonnull Entity entity) {
        return entity.attackEntityFrom(DamageSource.causeMobDamage(this), 2);
    }

}
