/*
 * KomodoEntity.java
 *
 *  Copyright (c) 2017 Michael Sheppard
 *
 * =====GPLv3===========================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.lizards.common;

import net.minecraft.block.BlockState;
import net.minecraft.entity.*;
import net.minecraft.entity.ai.goal.*;
import net.minecraft.entity.passive.PigEntity;
import net.minecraft.entity.passive.SheepEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.ArrowEntity;
import net.minecraft.network.datasync.DataParameter;
import net.minecraft.network.datasync.DataSerializers;
import net.minecraft.network.datasync.EntityDataManager;
import net.minecraft.pathfinding.PathNodeType;
import net.minecraft.util.DamageSource;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.SoundEvents;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import net.minecraft.world.biome.Biome;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public final class KomodoEntity extends CreatureEntity {

    private final float WIDTH = 1.0f;
    private final float HEIGHT = 0.3f;
    private final float scaleFactor;
    public EntitySize lizardSize = new EntitySize(1.0f, 0.3f, false);
    private static final DataParameter<Float> health = EntityDataManager.createKey(KomodoEntity.class, DataSerializers.FLOAT);

    public KomodoEntity(EntityType<? extends KomodoEntity> entity, World world) {
        super(entity, world);
        setPathPriority(PathNodeType.WATER, 0.0f);

        if (ConfigHandler.CommonConfig.useRandomScaling()) {
            float scale = rand.nextFloat();
            scaleFactor = scale < 0.55F ? 1.0F : scale;
        } else {
            scaleFactor = 1.0F;
        }
        lizardSize.scale(WIDTH * getScaleFactor(), HEIGHT * getScaleFactor());
    }

    @SuppressWarnings("unused")
    public KomodoEntity(World world) {
        this(LizardMod.RegistryEvents.KOMODO, world);
    }

    @Override
    protected void registerGoals() {
        goalSelector.addGoal(1, new SwimGoal(this));
        goalSelector.addGoal(1, new PanicGoal(this, 0.38));
        goalSelector.addGoal(3, new LeapAtTargetGoal(this, 0.4F));
        goalSelector.addGoal(4, new MeleeAttackGoal(this, 1.0D, true));
        goalSelector.addGoal(8, new RandomWalkingGoal(this, 1.0));
        goalSelector.addGoal(9, new LookAtGoal(this, PlayerEntity.class, 10.0F));
        goalSelector.addGoal(10, new LookRandomlyGoal(this));

        targetSelector.addGoal(1, new HurtByTargetGoal(this));
        targetSelector.addGoal(2, new NearestAttackableTargetGoal<>(this, PigEntity.class, false, true));
        targetSelector.addGoal(2, new NearestAttackableTargetGoal<>(this, SheepEntity.class, false, true));
    }


    public float getScaleFactor() {
        return scaleFactor;
    }

    @Override
    public boolean canDespawn(double distanceToPlayer) {
        return false;
    }

    @Override
    protected void registerAttributes() {
        super.registerAttributes();
        getAttribute(SharedMonsterAttributes.MOVEMENT_SPEED).setBaseValue(0.3D);
        getAttribute(SharedMonsterAttributes.MAX_HEALTH).setBaseValue(10.0D);
        getAttributes().registerAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).setBaseValue(2.0D);
    }

    @Override
    protected void registerData() {
        super.registerData();
        dataManager.register(health, getHealth());
    }

    @Override
    protected float getStandingEyeHeight(Pose pose, EntitySize size) {
        return HEIGHT * 0.9f;
    }

    @Nonnull
    @Override
    public EntityType<?> getType() {
        return LizardMod.RegistryEvents.KOMODO;
    }

    @Nonnull
    @Override
    public EntitySize getSize(Pose p) {
        return new EntitySize(WIDTH * scaleFactor, HEIGHT * scaleFactor, false);
    }

    @Nonnull
    @Override
    public ResourceLocation getLootTable() {
        return new ResourceLocation(LizardMod.MODID, LizardMod.KOMODO_NAME);
    }

    @Override
    public void setAttackTarget(@Nullable LivingEntity livingEntity) {
        super.setAttackTarget(livingEntity);
    }

    @Override
    public void livingTick() {
        super.livingTick();
    }

    @Override
    public void tick() {
        // kill in cold biomes
        Vec3d v = getPositionVec();
        BlockPos bp = new BlockPos(v.x, v.y, v.z);
        Biome biome = world.getBiome(bp);
        if (biome.getTemperature(bp) <= 0.25) {
            attackEntityFrom(DamageSource.STARVE, 4.0f);
        }
        super.tick();
    }

    @Override
    public int getMaxSpawnedInChunk() {
        return 8;
    }

    @Override
    public int getTalkInterval() {
        return ConfigHandler.CommonConfig.getTalkInterval();
    }

    @Override
    protected float getSoundVolume() {
        return (float)ConfigHandler.CommonConfig.getTalkVolume();
    }

    @Override
    protected SoundEvent getAmbientSound() {
        return LizardMod.RegistryEvents.VARANUS_HISS;
    }

    @Override
    protected SoundEvent getHurtSound(DamageSource damageSourceIn) {
        return LizardMod.RegistryEvents.VARANUS_HURT;
    }

    @Override
    protected SoundEvent getDeathSound() {
        return LizardMod.RegistryEvents.VARANUS_HURT;
    }

    protected SoundEvent getStepSound() {
        return SoundEvents.ENTITY_PIG_STEP;
    }

    @Override
    protected void playStepSound(@Nonnull BlockPos pos, BlockState blockIn) {
        playSound(getStepSound(), 0.15F, 1.0F);
    }

    @Override
    public boolean attackEntityAsMob(@Nonnull Entity entity) {
        boolean flag = entity.attackEntityFrom(DamageSource.causeMobDamage(this), (float) ((int) getAttribute(SharedMonsterAttributes.ATTACK_DAMAGE).getValue()));

        if (flag) {
            applyEnchantments(this, entity);
        }

        return flag;
    }

    @Override
    public boolean attackEntityFrom(@Nonnull DamageSource source, float amount) {
        if (isInvulnerable()) {
            return false;
        } else {
            Entity entity = source.getTrueSource();

            if (entity != null && !(entity instanceof PlayerEntity) && !(entity instanceof ArrowEntity)) {
                amount = (amount + 1.0F) / 2.0F;
            }

            return super.attackEntityFrom(source, amount);
        }
    }

    @Override
    protected void updateAITasks() {
        dataManager.set(health, getHealth());
    }

}
