/*
 * LizardMod.java
 *
 *  Copyright (c) 2017 Michael Sheppard
 *
 * =====GPLv3===========================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.lizards.common;

import com.lizards.client.*;
import net.minecraft.entity.*;
import net.minecraft.item.Food;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.common.BiomeDictionary;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.client.registry.RenderingRegistry;
import net.minecraftforge.fml.common.Mod;

import java.util.*;

import net.minecraftforge.common.BiomeDictionary.Type;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.IForgeRegistryEntry;
import net.minecraftforge.registries.ObjectHolder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


@SuppressWarnings("unused")
@Mod(LizardMod.MODID)
public class LizardMod {

    public static final String MODID = "lizardmod";
    public static final String LIZARDS_NAME = "lizards";

    private static final Logger LOGGER = LogManager.getLogger(LizardMod.MODID);

    // List of always excluded biome types
    private static final List<Type> excludedBiomeTypes = new ArrayList<>(Arrays.asList(
            Type.END,
            Type.NETHER,
            Type.VOID,
            Type.COLD,
            Type.OCEAN,
            Type.CONIFEROUS,
            Type.MOUNTAIN,
            Type.MUSHROOM,
            Type.SNOWY
    ));

    public static final String COOKED_REPTILEMEAT = "cooked_reptilemeat";
    public static final String REPTILEMEAT = "reptilemeat";
    public static final String REPTILE_HIDE = "reptile_hide";

    public static final String KOMODO_NAME = "komodo";
    public static final String SAVANNA_NAME = "savanna";
    public static final String PERENTIE_NAME = "perentie";
    public static final String GRISEUS_NAME = "griseus";
    public static final String SALVADORII_NAME = "salvadorii";
    public static final String LACE_NAME = "lace";
    public static final String IGUANA_NAME = "iguana";
    public static final String CHAMELEON_NAME = "chameleon";

    public static final String KOMODO_SPAWN_EGG = "komodo_spawn_egg";
    public static final String SAVANNA_SPAWN_EGG = "savanna_spawn_egg";
    public static final String PERENTIE_SPAWN_EGG = "perentie_spawn_egg";
    public static final String GRISEUS_SPAWN_EGG = "griseus_spawn_egg";
    public static final String SALVADORII_SPAWN_EGG = "salvadorii_spawn_egg";
    public static final String LACE_SPAWN_EGG = "lace_spawn_egg";
    public static final String IGUANA_SPAWN_EGG = "iguana_spawn_egg";
    public static final String CHAMELEON_SPAWN_EGG = "chameleon_spawn_egg";

    public static final String HISS_SOUND_NAME = "varanus.hiss";
    public static final String HURT_SOUND_NAME = "varanus.hurt";

    public LizardMod() {
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::initClient);

        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, ConfigHandler.commonSpec);

        MinecraftForge.EVENT_BUS.register(this);
    }

    public void setup(final FMLCommonSetupEvent event) {
        ConfigHandler.loadConfig();

        registerSpawns();
        MinecraftForge.EVENT_BUS.register(new SpawnCheck());
    }

    public void initClient(final FMLClientSetupEvent event) {
        RenderingRegistry.registerEntityRenderingHandler(RegistryEvents.KOMODO, KomodoRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(RegistryEvents.SAVANNA, SavannaRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(RegistryEvents.PERENTIE, PerentieRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(RegistryEvents.GRISEUS, GriseusRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(RegistryEvents.SALVADORII, SalvadoriiRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(RegistryEvents.LACE, LaceRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(RegistryEvents.IGUANA, IguanaRenderer::new);
        RenderingRegistry.registerEntityRenderingHandler(RegistryEvents.CHAMELEON, ChameleonRenderer::new);
    }

    @Mod.EventBusSubscriber(modid = LizardMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
    @ObjectHolder(LizardMod.MODID)
    public static class RegistryEvents {
        public static final EntityType<ChameleonEntity> CHAMELEON = EntityType.Builder.<ChameleonEntity>create(ChameleonEntity::new, EntityClassification.CREATURE)
                .setTrackingRange(80).build(MODID);
        public static final EntityType<GriseusEntity> GRISEUS = EntityType.Builder.<GriseusEntity>create(GriseusEntity::new, EntityClassification.CREATURE)
                .setTrackingRange(80).build(MODID);
        public static final EntityType<IguanaEntity> IGUANA = EntityType.Builder.<IguanaEntity>create(IguanaEntity::new, EntityClassification.CREATURE)
                .setTrackingRange(80).build(MODID);
        public static final EntityType<KomodoEntity> KOMODO = EntityType.Builder.<KomodoEntity>create(KomodoEntity::new, EntityClassification.CREATURE)
                .setTrackingRange(80).build(MODID);
        public static final EntityType<LaceEntity> LACE = EntityType.Builder.<LaceEntity>create(LaceEntity::new, EntityClassification.CREATURE)
                .setTrackingRange(80).build(MODID);
        public static final EntityType<PerentieEntity> PERENTIE = EntityType.Builder.<PerentieEntity>create(PerentieEntity::new, EntityClassification.CREATURE)
                .setTrackingRange(80).build(MODID);
        public static final EntityType<SalvadoriiEntity> SALVADORII = EntityType.Builder.<SalvadoriiEntity>create(SalvadoriiEntity::new, EntityClassification.CREATURE)
                .setTrackingRange(80).build(MODID);
        public static final EntityType<SavannaEntity> SAVANNA = EntityType.Builder.<SavannaEntity>create(SavannaEntity::new, EntityClassification.CREATURE)
                .setTrackingRange(80).build(MODID);

        public static final SoundEvent VARANUS_HISS = new SoundEvent(new ResourceLocation(LizardMod.MODID, LizardMod.HISS_SOUND_NAME));
        public static final SoundEvent VARANUS_HURT = new SoundEvent(new ResourceLocation(LizardMod.MODID, LizardMod.HURT_SOUND_NAME));

        @SubscribeEvent
        public static void onEntityRegistry(final RegistryEvent.Register<EntityType<?>> event) {
            event.getRegistry().registerAll(
                    setup(RegistryEvents.CHAMELEON, LizardMod.CHAMELEON_NAME),
                    setup(RegistryEvents.GRISEUS, LizardMod.GRISEUS_NAME),
                    setup(RegistryEvents.IGUANA, LizardMod.IGUANA_NAME),
                    setup(RegistryEvents.KOMODO, LizardMod.KOMODO_NAME),
                    setup(RegistryEvents.LACE, LizardMod.LACE_NAME),
                    setup(RegistryEvents.PERENTIE, LizardMod.PERENTIE_NAME),
                    setup(RegistryEvents.SALVADORII, LizardMod.SALVADORII_NAME),
                    setup(RegistryEvents.SAVANNA, LizardMod.SAVANNA_NAME)
            );
            getLogger().debug("onEntityRegistry finished");
        }

        @SubscribeEvent
        public static void onItemRegistry(final RegistryEvent.Register<Item> event) {
            event.getRegistry().registerAll(
                    setup(new VaranusMeatItem(new Item.Properties().group(ItemGroup.FOOD)
                            .food((new Food.Builder()).hunger(8).saturation(0.8f).meat().build())), LizardMod.COOKED_REPTILEMEAT),

                    setup(new VaranusMeatItem(new Item.Properties().group(ItemGroup.FOOD)
                            .food((new Food.Builder()).hunger(3).saturation(0.3F).meat().build())), LizardMod.REPTILEMEAT),

                    setup(new Item(new Item.Properties().group(ItemGroup.MATERIALS)), LizardMod.REPTILE_HIDE),

                    setup(new SpawnEggItem(CHAMELEON, 0xB22222, 0x228B22, new Item.Properties().group(ItemGroup.MISC)), LizardMod.CHAMELEON_SPAWN_EGG),
                    setup(new SpawnEggItem(GRISEUS, 0xCD853F, 0xDEB887, new Item.Properties().group(ItemGroup.MISC)), LizardMod.GRISEUS_SPAWN_EGG),
                    setup(new SpawnEggItem(IGUANA, 0x00CD00, 0xC0FF3E, new Item.Properties().group(ItemGroup.MISC)), LizardMod.IGUANA_SPAWN_EGG),
                    setup(new SpawnEggItem(KOMODO, 0x006400, 0x98FB98, new Item.Properties().group(ItemGroup.MISC)), LizardMod.KOMODO_SPAWN_EGG),
                    setup(new SpawnEggItem(LACE, 0x0A0A0A, 0xABABAB, new Item.Properties().group(ItemGroup.MISC)), LizardMod.LACE_SPAWN_EGG),
                    setup(new SpawnEggItem(PERENTIE, 0x363636, 0x7F7F7F, new Item.Properties().group(ItemGroup.MISC)), LizardMod.PERENTIE_SPAWN_EGG),
                    setup(new SpawnEggItem(SALVADORII, 0x008BCC, 0xA2CD5A, new Item.Properties().group(ItemGroup.MISC)), LizardMod.SALVADORII_SPAWN_EGG),
                    setup(new SpawnEggItem(SAVANNA, 0x8B8989, 0xCDC5BF, new Item.Properties().group(ItemGroup.MISC)), LizardMod.SAVANNA_SPAWN_EGG)
            );
            getLogger().debug("onItemRegistry complete");
        }

        @SubscribeEvent
        public static void onSoundRegistry(final RegistryEvent.Register<SoundEvent> event) {
            VARANUS_HISS.setRegistryName(LizardMod.HISS_SOUND_NAME);
            VARANUS_HURT.setRegistryName(LizardMod.HURT_SOUND_NAME);

            getLogger().debug("onSoundRegistry finished");
        }

        public static <T extends IForgeRegistryEntry<T>> T setup(final T entry, final String name) {
            return setup(entry, new ResourceLocation(LizardMod.MODID, name));
        }

        public static <T extends IForgeRegistryEntry<T>> T setup(final T entry, final ResourceLocation registryName) {
            entry.setRegistryName(registryName);
            return entry;
        }
    }

    private void registerSpawns() {
        Biome[] forestBiomes = getBiomes(true, Type.FOREST);
        Biome[] jungleBiomes = getBiomes(false, Type.HOT, Type.WET);
        Biome[] plainsBiomes = getBiomes(true, Type.PLAINS);
        Biome[] savannaBiomes = getBiomes(false, Type.SAVANNA);
        Biome[] desertBiomes = getBiomes(false, Type.HOT, Type.DRY, Type.SANDY);
        Biome[] combinedBiomes = getBiomes(true, Type.FOREST, Type.SAVANNA);

        int minSpawn = ConfigHandler.CommonConfig.getMinSpawn();
        int maxSpawn = ConfigHandler.CommonConfig.getMaxSpawn();

        registerEntitySpawn(RegistryEvents.CHAMELEON, plainsBiomes, ConfigHandler.CommonConfig.getChameleonSpawnProb(), minSpawn, maxSpawn);
        registerEntitySpawn(RegistryEvents.GRISEUS, desertBiomes, ConfigHandler.CommonConfig.getGriseusSpawnProb(), minSpawn, maxSpawn);
        registerEntitySpawn(RegistryEvents.IGUANA, jungleBiomes, ConfigHandler.CommonConfig.getIguanaSpawnProb(), minSpawn, maxSpawn);
        registerEntitySpawn(RegistryEvents.KOMODO, combinedBiomes, ConfigHandler.CommonConfig.getKomodoSpawnProb(), minSpawn, maxSpawn);
        registerEntitySpawn(RegistryEvents.LACE, forestBiomes, ConfigHandler.CommonConfig.getLaceSpawnProb(), minSpawn, maxSpawn);
        registerEntitySpawn(RegistryEvents.PERENTIE, forestBiomes, ConfigHandler.CommonConfig.getPerentieSpawnProb(), minSpawn, maxSpawn);
        registerEntitySpawn(RegistryEvents.SALVADORII, combinedBiomes, ConfigHandler.CommonConfig.getCrocMonitorSpawnProb(), minSpawn, maxSpawn);
        registerEntitySpawn(RegistryEvents.SAVANNA, savannaBiomes, ConfigHandler.CommonConfig.getSavannaSpawnProb(), minSpawn, maxSpawn);
    }

    private Biome[] getBiomes(boolean orTypes, Type... types) {
        LinkedList<Biome> list = new LinkedList<>();
        Collection<Biome> biomes = ForgeRegistries.BIOMES.getValues();

        for (Biome biome : biomes) {
            Set<Type> bTypes = BiomeDictionary.getTypes(biome);

            // we exclude certain biomes, i.e., lizards, monitors are ectothermic, no cold biomes
            if (excludeThisBiome(bTypes)) {
                continue;
            }
            // process remaining biomes
            if (orTypes) { // add any biome that contains any of the listed types (logical OR)
                for (Type t : types) {
                    if (BiomeDictionary.hasType(biome, t)) {
                        if (!list.contains(biome)) {
                            LizardMod.getLogger().info("Adding " + biome.getRegistryName() + " biome for spawning");
                            list.add(biome);
                        }
                    }
                }
            } else { // add any biome that contains all the types listed (logical AND)
                int count = types.length;
                int shouldAdd = 0;
                for (Type t : types) {
                    if (BiomeDictionary.hasType(biome, t)) {
                        shouldAdd++;
                    }
                }
                if (!list.contains(biome) && shouldAdd == count) {
                    LizardMod.getLogger().info("Adding " + biome.getRegistryName() + " biome for spawning");
                    list.add(biome);
                }
            }
        }
        return list.toArray(new Biome[0]);
    }

    private boolean excludeThisBiome(Set<Type> types) {
        boolean excludeBiome = false;
        for (Type ex : excludedBiomeTypes) {
            if (types.contains(ex)) {
                excludeBiome = true;
                break;
            }
        }
        return excludeBiome;
    }

    private void registerEntitySpawn(EntityType<? extends LivingEntity> type, Biome[] biomes, int spawnProb, int minSpawn, int maxSpawn) {
        if (spawnProb <= 0) {
            // do not spawn this entity
            return;
        }

        for (Biome bgb : biomes) {
            Biome biome = ForgeRegistries.BIOMES.getValue(bgb.getRegistryName());
            if (biome != null) {
                biome.getSpawns(EntityClassification.CREATURE).add(new Biome.SpawnListEntry(type, spawnProb, minSpawn, maxSpawn));
            }
        }
    }

    public static Logger getLogger() {
        return LOGGER;
    }
}
