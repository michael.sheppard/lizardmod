/*
 * ConfigHandler.java
 *
 *  Copyright (c) 2017 Michael Sheppard
 *
 * =====GPLv3===========================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.lizards.common;

import com.electronwill.nightconfig.core.file.CommentedFileConfig;
import net.minecraftforge.common.ForgeConfigSpec;
import org.apache.commons.lang3.tuple.Pair;

import java.nio.file.Paths;

public class ConfigHandler {

    public static void loadConfig() {
        CommentedFileConfig.builder(Paths.get("config", LizardMod.LIZARDS_NAME, LizardMod.MODID + ".toml")).build();
    }

    public static class CommonConfig {
        public static ForgeConfigSpec.IntValue komodoSpawnProb;
        public static ForgeConfigSpec.IntValue griseusSpawnProb;
        public static ForgeConfigSpec.IntValue laceSpawnProb;
        public static ForgeConfigSpec.IntValue perentieSpawnProb;
        public static ForgeConfigSpec.IntValue savannaSpawnProb;
        public static ForgeConfigSpec.IntValue iguanaSpawnProb;
        public static ForgeConfigSpec.IntValue chameleonSpawnProb;
        public static ForgeConfigSpec.IntValue crocMonitorSpawnProb;
        public static ForgeConfigSpec.IntValue minSpawn;
        public static ForgeConfigSpec.IntValue maxSpawn;
        public static ForgeConfigSpec.IntValue talkInterval;
        public static ForgeConfigSpec.DoubleValue talkvolume;
        public static ForgeConfigSpec.BooleanValue randomScale;

        public CommonConfig(ForgeConfigSpec.Builder builder) {
            builder.comment("Lizard Mod Config")
                    .push("CommonConfig");

            minSpawn = builder
                    .comment("Minimum number of lizards to spawn at one time")
                    .translation("config.lizards.minSpawn")
                    .defineInRange("minSpawn", 1, 1, 5);

            maxSpawn = builder
                    .comment("Maximum number of lizards to spawn at one time")
                    .translation("config.lizards.maxSpawn")
                    .defineInRange("maxSpawn", 4, 1, 10);

            komodoSpawnProb = builder
                    .comment("Spawn Probability Set to zero to disable spawning of this entity")
                    .translation("config.lizards.komodoSpawnProb")
                    .defineInRange("komodoSpawnProb", 10, 0, 100);

            griseusSpawnProb = builder
                    .comment("Spawn Probability Set to zero to disable spawning of this entity")
                    .translation("config.lizards.griseusSpawnProb")
                    .defineInRange("griseusSpawnProb", 10, 0, 100);

            laceSpawnProb = builder
                    .comment("Spawn Probability Set to zero to disable spawning of this entity")
                    .translation("config.lizards.laceSpawnProb")
                    .defineInRange("laceSpawnProb", 10, 0, 100);

            perentieSpawnProb = builder
                    .comment("Spawn Probability Set to zero to disable spawning of this entity")
                    .translation("config.lizards.perentieSpawnProb")
                    .defineInRange("perentieSpawnProb", 10, 0, 100);

            savannaSpawnProb = builder
                    .comment("Spawn Probability Set to zero to disable spawning of this entity")
                    .translation("config.lizards.savannaSpawnProb")
                    .defineInRange("savannaSpawnProb", 10, 0, 100);

            iguanaSpawnProb = builder
                    .comment("Spawn Probability Set to zero to disable spawning of this entity")
                    .translation("config.lizards.iguanaSpawnProb")
                    .defineInRange("iguanaSpawnProb", 10, 0, 100);

            chameleonSpawnProb = builder
                    .comment("Spawn Probability Set to zero to disable spawning of this entity")
                    .translation("config.lizards.chameleonSpawnProb")
                    .defineInRange("chameleonSpawnProb", 10, 1, 100);

            crocMonitorSpawnProb = builder
                    .comment("Spawn Probability Set to zero to disable spawning of this entity")
                    .translation("config.lizards.crocMonitorSpawnProb")
                    .defineInRange("crocMonitorSpawnProb", 10, 0, 100);

            talkInterval = builder
                    .comment("time interval between ambient sounds")
                    .translation("config.lizards.talkInterval")
                    .defineInRange("talkInterval", 320, 0, 1000);

            talkvolume = builder
                    .comment("volume of reptile sounds")
                    .translation("config.lizards.talkvolume")
                    .defineInRange("talkvolume", 0.3, 0.0, 1.0);

            randomScale = builder
                    .comment("Set to false to disable random scaling of monitors, default is true.")
                    .translation("config.lizards.randomScale")
                    .define("randomScale", true);

            builder.pop();
        }

        public static boolean useRandomScaling() {
            return randomScale.get();
        }

        public static int getKomodoSpawnProb() {
            return komodoSpawnProb.get();
        }

        public static int getGriseusSpawnProb() {
            return griseusSpawnProb.get();
        }

        public static int getLaceSpawnProb() {
            return laceSpawnProb.get();
        }

        public static int getPerentieSpawnProb() {
            return perentieSpawnProb.get();
        }

        public static int getSavannaSpawnProb() {
            return savannaSpawnProb.get();
        }

        public static int getIguanaSpawnProb() {
            return iguanaSpawnProb.get();
        }

        public static int getChameleonSpawnProb() {
            return chameleonSpawnProb.get();
        }

        public static int getCrocMonitorSpawnProb() {
            return crocMonitorSpawnProb.get();
        }

        public static int getMinSpawn() {
            return minSpawn.get();
        }

        public static int getMaxSpawn() {
            return maxSpawn.get();
        }

        public static int getTalkInterval() {
            return talkInterval.get();
        }

        public static double getTalkVolume() {
            return talkvolume.get();
        }
    }

    static final ForgeConfigSpec commonSpec;
    public static final CommonConfig COMMON_CONFIG;
    static {
        final Pair<CommonConfig, ForgeConfigSpec> specPair = new ForgeConfigSpec.Builder().configure(CommonConfig::new);
        commonSpec = specPair.getRight();
        COMMON_CONFIG = specPair.getLeft();
    }
}
