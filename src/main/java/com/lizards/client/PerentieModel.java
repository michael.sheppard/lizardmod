/*
 * PerentieModel.java
 *
 *  Copyright (c) 2017 Michael Sheppard
 *
 * =====GPLv3===========================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.lizards.client;

import com.google.common.collect.ImmutableList;
import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

import javax.annotation.Nonnull;


public class PerentieModel<T extends Entity> extends SegmentedModel<T> {

    public ModelRenderer perentieBody;
    public ModelRenderer perentieHead;
    public ModelRenderer perentieLeg1;
    public ModelRenderer perentieLeg2;
    public ModelRenderer perentieLeg3;
    public ModelRenderer perentieLeg4;
    ModelRenderer perentieTail;

    public PerentieModel() {
        float yPos = 19F;

        perentieBody = new ModelRenderer(this, 21, 16);
        perentieBody.addBox(-3F, -2F, -5F, 6, 4, 10);
        perentieBody.setRotationPoint(0.0F, yPos, 0.0F);

        perentieHead = new ModelRenderer(this, 0, 0);
        perentieHead.addBox(-2F, -2F, -6F, 4, 4, 6);
        perentieHead.setRotationPoint(0F, yPos, -5F);

        perentieLeg1 = new ModelRenderer(this, 56, 1);
        perentieLeg1.addBox(-1F, 0F, -1F, 2, 5, 2);
        perentieLeg1.setRotationPoint(4F, yPos, -4F);

        perentieLeg2 = new ModelRenderer(this, 56, 1);
        perentieLeg2.addBox(-1F, 0F, -1F, 2, 5, 2);
        perentieLeg2.setRotationPoint(4F, yPos, 4F);

        perentieLeg3 = new ModelRenderer(this, 56, 1);
        perentieLeg3.mirror = true;
        perentieLeg3.addBox(-1F, 0F, -1F, 2, 5, 2);
        perentieLeg3.setRotationPoint(-4F, yPos, -4F);

        perentieLeg4 = new ModelRenderer(this, 56, 1);
        perentieLeg4.mirror = true;
        perentieLeg4.addBox(-1F, 0F, -1F, 2, 5, 2);
        perentieLeg4.setRotationPoint(-4F, yPos, 4F);

        perentieTail = new ModelRenderer(this, 17, 12);
        perentieTail.addBox(-1F, -1F, 0F, 2, 2, 18);
        perentieTail.setRotationPoint(0F, yPos, 4F);
        perentieTail.rotateAngleX = 6.021385919380437F;
    }

    @Override
    public void setRotationAngles(@Nonnull T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
        perentieHead.rotateAngleX = headPitch / 57.29578F;
        perentieHead.rotateAngleY = headPitch / 57.29578F;

        perentieLeg1.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
        perentieLeg2.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
        perentieLeg3.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
        perentieLeg4.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;

        perentieTail.rotateAngleY = MathHelper.cos(limbSwing * 0.6662F) * 0.4F * limbSwingAmount;
    }

    @Override
    @Nonnull
    public Iterable<ModelRenderer> getParts() {
        return ImmutableList.of(perentieHead, perentieBody, perentieLeg1, perentieLeg2, perentieLeg3, perentieLeg4, perentieTail);
    }
}
