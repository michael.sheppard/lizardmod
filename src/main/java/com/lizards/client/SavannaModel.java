/*
 * SavannaModel.java
 *
 *  Copyright (c) 2017 Michael Sheppard
 *
 * =====GPLv3===========================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.lizards.client;

import com.google.common.collect.ImmutableList;
import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

import javax.annotation.Nonnull;


public class SavannaModel<T extends Entity> extends SegmentedModel<T> {

    public ModelRenderer savannaBody;
    public ModelRenderer savannaHead;
    public ModelRenderer savannaLeg1;
    public ModelRenderer savannaLeg2;
    public ModelRenderer savannaLeg3;
    public ModelRenderer savannaLeg4;
    ModelRenderer savannaTail;

    public SavannaModel() {
        float yPos = 19F;

        savannaBody = new ModelRenderer(this, 30, 2); // 21 16
        savannaBody.addBox(-3F, -2F, -5F, 6, 4, 10);
        savannaBody.setRotationPoint(0.0F, yPos, 0.0F);

        savannaHead = new ModelRenderer(this, 0, 0);
        savannaHead.addBox(-2F, -2F, -6F, 4, 4, 6);
        savannaHead.setRotationPoint(0F, yPos, -5F);

        savannaLeg1 = new ModelRenderer(this, 56, 1);
        savannaLeg1.addBox(-1F, 0F, -1F, 2, 5, 2);
        savannaLeg1.setRotationPoint(4F, yPos, -4F);

        savannaLeg2 = new ModelRenderer(this, 56, 1);
        savannaLeg2.addBox(-1F, 0F, -1F, 2, 5, 2);
        savannaLeg2.setRotationPoint(4F, yPos, 4F);

        savannaLeg3 = new ModelRenderer(this, 56, 1);
        savannaLeg3.mirror = true;
        savannaLeg3.addBox(-1F, 0F, -1F, 2, 5, 2);
        savannaLeg3.setRotationPoint(-4F, yPos, -4F);

        savannaLeg4 = new ModelRenderer(this, 56, 1);
        savannaLeg4.mirror = true;
        savannaLeg4.addBox(-1F, 0F, -1F, 2, 5, 2);
        savannaLeg4.setRotationPoint(-4F, yPos, 4F);

        savannaTail = new ModelRenderer(this, 4, 12); // 17 12
        savannaTail.addBox(-1F, -1F, 0F, 2, 2, 18);
        savannaTail.setRotationPoint(0F, yPos, 4F);
        savannaTail.rotateAngleX = 6.021385919380437F;
    }

    @Override
    public void setRotationAngles(@Nonnull T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
        savannaHead.rotateAngleX = headPitch / 57.29578F;
        savannaHead.rotateAngleY = headPitch / 57.29578F;

        savannaLeg1.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
        savannaLeg2.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
        savannaLeg3.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
        savannaLeg4.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;

        savannaTail.rotateAngleY = MathHelper.cos(limbSwing * 0.6662F) * 0.4F * limbSwingAmount;
    }

    @Override
    @Nonnull
    public Iterable<ModelRenderer> getParts() {
        return ImmutableList.of(savannaHead, savannaBody, savannaLeg1, savannaLeg2, savannaLeg3, savannaLeg4, savannaTail);
    }
}
