/*
 * SalvadoriiModel.java
 *
 *  Copyright (c) 2017 Michael Sheppard
 *
 * =====GPLv3===========================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.lizards.client;

import com.google.common.collect.ImmutableList;
import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;

import net.minecraft.util.math.MathHelper;

import javax.annotation.Nonnull;

public class SalvadoriiModel<T extends Entity> extends SegmentedModel<T> {

    public ModelRenderer salvadoriiBody;
    public ModelRenderer salvadoriiHead;
    public ModelRenderer salvadoriiLeg1;
    public ModelRenderer salvadoriiLeg2;
    public ModelRenderer salvadoriiLeg3;
    public ModelRenderer salvadoriiLeg4;
    ModelRenderer salvadoriiTail;

    public SalvadoriiModel() {
        float yPos = 19F;

        salvadoriiBody = new ModelRenderer(this, 21, 16);
        salvadoriiBody.addBox(-3F, -2F, -5F, 6, 4, 10);
        salvadoriiBody.setRotationPoint(0.0F, yPos, 0.0F);

        salvadoriiHead = new ModelRenderer(this, 0, 0);
        salvadoriiHead.addBox(-2F, -2F, -6F, 4, 4, 6);
        salvadoriiHead.setRotationPoint(0F, yPos, -5F);

        salvadoriiLeg1 = new ModelRenderer(this, 56, 1);
        salvadoriiLeg1.addBox(-1F, 0F, -1F, 2, 5, 2);
        salvadoriiLeg1.setRotationPoint(4F, yPos, -4F);

        salvadoriiLeg2 = new ModelRenderer(this, 56, 1);
        salvadoriiLeg2.addBox(-1F, 0F, -1F, 2, 5, 2);
        salvadoriiLeg2.setRotationPoint(4F, yPos, 4F);

        salvadoriiLeg3 = new ModelRenderer(this, 56, 1);
        salvadoriiLeg3.mirror = true;
        salvadoriiLeg3.addBox(-1F, 0F, -1F, 2, 5, 2);
        salvadoriiLeg3.setRotationPoint(-4F, yPos, -4F);

        salvadoriiLeg4 = new ModelRenderer(this, 56, 1);
        salvadoriiLeg4.mirror = true;
        salvadoriiLeg4.addBox(-1F, 0F, -1F, 2, 5, 2);
        salvadoriiLeg4.setRotationPoint(-4F, yPos, 4F);

        salvadoriiTail = new ModelRenderer(this, 17, 12);
        salvadoriiTail.addBox(-1F, -1F, 0F, 2, 2, 18);
        salvadoriiTail.setRotationPoint(0F, yPos, 4F);
        salvadoriiTail.rotateAngleX = 6.021385919380437F;
    }

    @Override
    public void setRotationAngles(@Nonnull T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
        salvadoriiHead.rotateAngleX = headPitch / 57.29578F;
        salvadoriiHead.rotateAngleY = headPitch / 57.29578F;

        salvadoriiLeg1.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
        salvadoriiLeg2.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
        salvadoriiLeg3.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
        salvadoriiLeg4.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;

        salvadoriiTail.rotateAngleY = MathHelper.cos(limbSwing * 0.6662F) * 0.4F * limbSwingAmount;
    }

    @Override
    @Nonnull
    public Iterable<ModelRenderer> getParts() {
        return ImmutableList.of(salvadoriiHead, salvadoriiBody, salvadoriiLeg1, salvadoriiLeg2, salvadoriiLeg3, salvadoriiLeg4, salvadoriiTail);
    }
}
