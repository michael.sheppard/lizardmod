/*
 * GriseusModel.java
 *
 *  Copyright (c) 2017 Michael Sheppard
 *
 * =====GPLv3===========================================================
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses.
 * =====================================================================
 */

package com.lizards.client;

import com.google.common.collect.ImmutableList;
import net.minecraft.client.renderer.entity.model.SegmentedModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;

import javax.annotation.Nonnull;

public class GriseusModel<T extends Entity> extends SegmentedModel<T> {

    public ModelRenderer griseusBody;
    public ModelRenderer griseusHead;
    public ModelRenderer griseusLeg1;
    public ModelRenderer griseusLeg2;
    public ModelRenderer griseusLeg3;
    public ModelRenderer griseusLeg4;
    ModelRenderer griseusTail;

    public GriseusModel() {
        float yPos = 19F;

        griseusBody = new ModelRenderer(this, 21, 16);
        griseusBody.addBox(-3F, -2F, -5F, 6, 4, 10);
        griseusBody.setRotationPoint(0.0F, yPos, 0.0F);

        griseusHead = new ModelRenderer(this, 0, 0);
        griseusHead.addBox(-2F, -2F, -6F, 4, 4, 6);
        griseusHead.setRotationPoint(0F, yPos, -5F);

        griseusLeg1 = new ModelRenderer(this, 56, 1);
        griseusLeg1.addBox(-1F, 0F, -1F, 2, 5, 2);
        griseusLeg1.setRotationPoint(4F, yPos, -4F);

        griseusLeg2 = new ModelRenderer(this, 56, 1);
        griseusLeg2.addBox(-1F, 0F, -1F, 2, 5, 2);
        griseusLeg2.setRotationPoint(4F, yPos, 4F);

        griseusLeg3 = new ModelRenderer(this, 56, 1);
        griseusLeg3.mirror = true;
        griseusLeg3.addBox(-1F, 0F, -1F, 2, 5, 2);
        griseusLeg3.setRotationPoint(-4F, yPos, -4F);

        griseusLeg4 = new ModelRenderer(this, 56, 1);
        griseusLeg4.mirror = true;
        griseusLeg4.addBox(-1F, 0F, -1F, 2, 5, 2);
        griseusLeg4.setRotationPoint(-4F, yPos, 4F);

        griseusTail = new ModelRenderer(this, 17, 12);
        griseusTail.addBox(-1F, -1F, 0F, 2, 2, 18);
        griseusTail.setRotationPoint(0F, yPos, 4F);
        griseusTail.rotateAngleX = 6.021385919380437F;

    }

    @Override
    public void setRotationAngles(@Nonnull T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
        griseusHead.rotateAngleX = headPitch / 57.29578F;
        griseusHead.rotateAngleY = headPitch / 57.29578F;

        griseusLeg1.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
        griseusLeg2.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
        griseusLeg3.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
        griseusLeg4.rotateAngleX = MathHelper.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;

        griseusTail.rotateAngleY = MathHelper.cos(limbSwing * 0.6662F) * 0.4F * limbSwingAmount;
    }

    @Override
    @Nonnull
    public Iterable<ModelRenderer> getParts() {
        return ImmutableList.of(griseusHead, griseusBody, griseusLeg1, griseusLeg2, griseusLeg3, griseusLeg4, griseusTail);
    }

}
